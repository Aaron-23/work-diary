## NFS存储迁移至GlusterFS

### 前提条件


- 为防止脑裂，建议使用最低3台节点制作3复制集的存储卷；
- 在进行存储迁移前，GluseterFS存储节点需先成为k8s集群中的node节点；
- 存储切换时请勿在平台继续操作，以免数据切换后不一致；
- GluseterFS所使用磁盘挂载在/data目录，推荐使用逻辑卷；
- 如果之前通过修改控制器方式修改过Rainbond组件相关数据，请提前做好备份，存储切换后会重置相关修改。

### 部署GlusterFS


将存储节点添加至当前k8s集群，参考Rainbond官方文档安装的k8s集群扩容节点命令如下

  ```bash
  docker exec -it kubeasz easzctl add-node NODE_IP
  ```

- 在k8s集群所有节点安装所需要的组件

  - Ubuntu16.04/18.04

  ```bash
  add-apt-repository ppa:gluster/glusterfs-7
  apt-get update
  apt-get install glusterfs-client -d 
  modprobe dm_thin_pool
  ```
  - CentOS 7
 
  ```bash
  yum -y install centos-release-gluster
  yum -y install glusterfs-client
  modprobe dm_thin_pool
  ```
  
  所有节点GlusterFS版本必须为7.*+，否则会出现无法挂载存储的情况；可通过执行`glusterfs --version`验证。

- 在GluseterFS节点划分分区并挂载磁盘

  ```bash
  # 查看可用磁盘
  fdisk -l
  # 分区并格式化
  mkfs.xfs  /dev/vdb1
  mkdir  -p /data
  echo "/dev/vdb1  /data  xfs  defaults 1 2" >>/etc/fstab
  # 挂载
  mount -a
  # 确定/data挂载
  df -h | grep data
  ```

  以下操作在k8s集群的任一master节点执行

- 获取对应项目

 ```bash
 git clone https://gitee.com/liu_shuai2573/gfs-k8s.git && cd gfs-k8s
 ```

- 设置标签使该节点只运行GFS

 ```bash
 #设置标签，调度glusterfs的pod至指定节点
 kubectl label node 10.10.10.71 10.10.10.72 10.10.10.73 storagenode=glusterfs
 #设置taint策略，使该节点只运行glusterfs，如需复用glusterfs节点请忽略该操作
 kubectl taint node 10.10.10.71 10.10.10.72 10.10.10.73 glusterfs=true:NoSchedule
  ```

- 运行GFS的相关控制器（视网络及性能情况而定，大约需要两分钟）

 ```bash
 kubectl create -f gluster-daemonset.yaml
 ```

- 检查是否在指定节点正常运行

 ```bash
 $ kubectl get pods -o wide --selector=glusterfs-node=daemonset
  NAME              READY   STATUS    RESTARTS   AGE    IP              NODE            NOMINATED NODE   READINESS GATES
  glusterfs-2k5rm   1/1     Running   0          52m    192.168.2.200   192.168.2.200   <none>           <none>
  glusterfs-mc6pg   1/1     Running   0          134m   192.168.2.22    192.168.2.22    <none>           <none>
  glusterfs-tgsn7   1/1     Running   0          134m   192.168.2.224   192.168.2.224   <none>           <none>
  ```

- 为GFS集群添加节点

  ```bash
  kubectl exec -ti glusterfs-2k5rm gluster peer probe 192.168.2.22
  kubectl exec -ti glusterfs-2k5rm gluster peer probe 192.168.2.224
  
  #检测是否添加成功，成功则显示Connected状态
  kubectl exec -ti glusterfs-2k5rm gluster peer status
  
  ```

- 创建服务账户并进行RBAC授权

  ```bash
  kubectl create -f rbac.yaml
  ```

### 配置k8s集群使用GFS

  - 创建provisioner资源

  ```bash
  kubectl create -f deployment.yaml
  ```

  - 创建storageclass资源

    修改`storageclass.yaml`文件中的`parameters.brickrootPaths`字段对应的值，将GlusterFS节点的IP替换掉。
  
```bash
kubectl create -f storageclass.yaml
```

  - 创建pvc验证
  
```bash
kubectl create -f pvc.yaml
#创建成功时STATUS为Bound
kubectl get pvc | grep gluster-simple-claim 
```

  - 创建pod验证
  
  运行正常时STATUS为Running
  
```bash
kubectl create -f pod.yaml
kubectl get po | grep gluster-simple-pod
#验证正常后将pod删除
kubectl delete -f pod.yaml
```


### 数据备份

数据备份是考虑到实际存储切换时使用原NFS存储向GFS同步数据可能会由于受到网络因素等影响导致较慢，所以事先将NFS中的数据拷贝至手动创建的GFS存储卷中，在最终切换时再从当前的GFS卷中向切换后自动创建的卷中同步数据，需要确保GFS存储磁盘容量能够容纳以下组件的数据*2份；若实际情况NFS向GFS同步数据较快，此步骤可以忽略。

需要备份的数据如下


- rbd-repo
- rbd-hub
- rbd-chaos
- rbd-cpt-grdata

使用NFS存储的数据默认存储在NFS所运行宿主机的 `/opt/rainbond/data/nfs` 目录下，根据PVC名字进行划分，将上述需要备份的文件进行备份。

- 手动创建一个存储卷，用作备份使用

```bash
$ kubectl exec -ti glusterfs-2k5rm gluster volume create backup replica 3  192.168.2.200:/data/backup 192.168.2.22:/data/backup 192.168.2.224:/data/backup
$ gluster volume start backup
```

```bash
#在原NFS节点挂载该卷，方便进行备份
mkdir /backup
mount.glusterfs  192.168.2.200:/backup  /backup
#获取上述需备份数据与NFS存储中所对应的文件夹名称（对比VOLUME名字与NFS存储中文件夹名字）
kubectl get pvc -n  rbd-system
#在原NFS节点使用rsync同步数据，可进行增量备份，使用此方式备份上述四份数据
rsync -azvP /opt/rainbond/data/nfs/组件PV名字    /backup/
```

### 切换存储

- 删除PVC

```bash
kubectl  get pvc  -A| grep rbd-system|grep rainbondvolumerwx|awk '{print$2}'|xargs kubectl delete pvc  -n rbd-system
```

- 删除PV

```bash
kubectl get pv |grep rainbondvolumerwx|awk '{print$1}'|xargs kubectl delete pv  -n rbd-system
```

如遇到无法删除参考[文档](https://www.cnblogs.com/weifeng1463/p/11490399.html)


- 修改rainbondvolume使用`glusterfs`存储

 修改`storageClassName`及`provisioner`的值。

```bash
kubectl edit rainbondvolume -n rbd-system

spec:
  csiPlugin:
    nfs: {}
  imageRepository: registry.cn-hangzhou.aliyuncs.com/goodrain
  storageClassName: glusterfs-simple
  storageClassParameters:
    provisioner: gluster.org/glusterfs-simple
  storageRequest: 1
```

- 删除控制器

> 如果之前通过修改控制器方式修改过Rainbond组件相关数据，pod正常创建后需再次修改，删除后会重置相关控制器参数。

删除后将会自动创建pod及对应存储pv，pvc

```bash
kubectl delete sts nfs-provisioner rbd-eventlog rbd-db rbd-etcd rbd-monitor rbd-repo -n rbd-system
kubectl delete ds rbd-chaos rbd-gateway rbd-node  -n rbd-system
kubectl delete deploy rbd-api rbd-app-ui rbd-hub rbd-mq rbd-webcli rbd-worker  -n rbd-system
```



- 拷贝数据

查询 `rbd-db，rbd-repo，rbd-hub，rbd-cpt-grdata` 新创建的pv名字

```bash
kubectl get pv -n rbd-system
```

通过临时挂载的方式将GFS存储卷挂载出来，将之前备份的数据分别拷贝到对应目录

> 以 rbd-hub 为例

```bash
mkdir  /rbd-hub
mount -t glusterfs 192.168.2.187:/{rbd-hub pv名字}   /rbd-hub
```

拷贝完毕后重启pod识别数据

```bash
kubectl get po -n rbd-system|awk '{print$1}'|xargs kubectl delete po -n rbd-system
```

**将平台运行组件的pv及pvc删除，并登录平台将所有应用重启**

```bash
kubectl get pvc -A|grep rainbondsssc|awk '{print$1,$2}'|xargs kubectl delete pvc $2 -n $1
kubectl get pv |grep rainbondsssc|awk '{print$1}'|xargs kubectl delete pv
```


### 验证

- 查看平台组件是否都处于running状态

```bash
$ kubectl get po -n  rbd-system
NAME                                         READY   STATUS    RESTARTS   AGE
dashboard-metrics-scraper-754cdcbbb6-lkxkb   1/1     Running   0          17m
kubernetes-dashboard-57b897f8df-wjj7s        1/1     Running   0          17m
rainbond-operator-0                          2/2     Running   0          13m
rbd-api-564f456468-7gj2j                     1/1     Running   1          17m
rbd-app-ui-64c7c55995-skjsg                  1/1     Running   3          17m
rbd-chaos-gq4xg                              1/1     Running   1          17m
rbd-db-0                                     2/2     Running   2          17m
rbd-etcd-0                                   1/1     Running   1          17m
rbd-eventlog-0                               1/1     Running   1          17m
rbd-gateway-msrsd                            1/1     Running   1          17m
rbd-hub-85b7b94846-b5kp6                     1/1     Running   1          17m
rbd-monitor-0                                1/1     Running   1          17m
rbd-mq-989c5549c-qtdqj                       1/1     Running   1          17m
rbd-node-7fbqt                               1/1     Running   0          17m
rbd-node-gqc5s                               1/1     Running   1          17m
rbd-node-mg62k                               1/1     Running   0          17m
rbd-repo-0                                   1/1     Running   1          17m
rbd-webcli-84969b7fc5-bhmgs                  1/1     Running   1          17m
rbd-worker-78b6dc8fc4-cjtn6                  1/1     Running   1          17m
```

- 检查使用存储的组件数据是否和迁移前一致

- 找到有使用共享存储的组件，通过`grctl service get`命令查看是否使用的是GlusterFS存储；通过endpoints及对应路径查看数据。

```bash
$ grctl service get gr1e52a5 -t v6j1tpha
PodVolume:
+----------------+------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| Volume         | Type | Volume Mount                                                                                                                                             |
+----------------+------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| /var/lib/mysql | nfs  | endpoints: glusterfs-simple-rbd-cpt-grdata                                                                                                               |
|                |      | path: pvc-c9e702f4-2a52-4f07-98b0-25488f9b585b/tenant/6dc4f0529b8c43d7a96db742029f43d1/service/37bf4d2a3608455d867c8289231e52a5/var/lib/mysql/gr1e52a5-0 |
|                |      | endpointsNamespace: rbd-system                                                                                                                           |
+----------------+------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
```

确认完毕后删除原nfs相关资源及数据。

```bash
kubectl delete pvc data-nfs-provisioner-0 -n  rbd-system
kubectl delete pv nfs-provisioner -n  rbd-system
```