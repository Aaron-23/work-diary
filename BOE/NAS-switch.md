
## NFS存储切换NFS服务端

### 存储挂载宿主机

首先在宿主机不同目录分别挂载两个 NAS，方便之后将旧NAS数据拷贝至新NAS中

```bash
$ mkdir /olddata
$ mount -t nfs  8230e488be-vjj26.cn-huhehaote.nas.aliyuncs.com:/ /olddata/
```

```bash
$ mkdir /newdata
$ mount -t nfs  8d52d4a696-wjt74.cn-huhehaote.nas.aliyuncs.com:/ /newdata/
```


### 创建新的storageclass

- 下载创建NFS驱动项目

```bash
$ git clone https://gitee.com/Aaron-23/external-storage && cd  ./external-storage/nfs-client/deploy
```

```bash
$ kubectl create -f rbac.yaml
```

- 修改挂载路径，创建deployment

```bash
$ vi deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nfs-client-provisioner-new

            - name: NFS_SERVER
              value: 10.10.10.60
            - name: NFS_PATH
              value: /ifs/kubernetes
      volumes:
        - name: nfs-client-root
          nfs:
            server: 10.10.10.60
            path: /ifs/kubernetes
            
$ kubectl create -f deployment.yaml
```

- 创建新的StorageClass

修改StorageClass名字后创建新的

```bash
$ vi class.yaml

apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: managed-nfs-storage-new
provisioner: fuseim.pri/ifs # or choose another name, must match deployment's env PROVISIONER_NAME'
parameters:
  archiveOnDelete: "false"
  
  
$ kubectl create -f class.yaml
```

- 确认已经创建完毕

```bash
$  kubectl get storageclass
NAME                      PROVISIONER                    AGE
managed-nfs-storage       fuseim.pri/ifs                 5h16m
managed-nfs-storage-new   fuseim.pri/ifs                 7s
rainbondslsc              rainbond.io/provisioner-sslc   4h56m
rainbondsssc              rainbond.io/provisioner-sssc   4h56m
```

## 删除原有存储

- 删除PVC

```bash
$ kubectl  get pvc  -A| grep rbd-system |awk '{print$2}'|xargs kubectl delete pvc  -n rbd-system

$ kubectl  get pvc  -A
```

- 删除PV

```bash
$ kubectl  get pv| grep rbd-system |awk '{print$1}'|xargs kubectl delete pv

$ kubectl  get pv
```

- 修改rainbondvolume使用新的nas存储

```bash
$ kubectl edit rainbondvolume -n rbd-system

  storageClassName: managed-nfs-storage-new
```

- 删除旧的 NFS deploy资源

```bash
kubectl delete deploy nfs-client-provisioner
```

- 删除控制器

将会自动创建pod及pvc

```bash
kubectl delete sts  rbd-db rbd-etcd rbd-monitor rbd-repo -n rbd-system
kubectl delete ds rbd-chaos rbd-gateway rbd-node  -n rbd-system
kubectl delete deploy rbd-api rbd-app-ui rbd-eventlog rbd-hub rbd-mq rbd-webcli rbd-worker  -n rbd-system
```

- 拷贝数据


推荐使用rsync可以增量拷贝，在最终nas切换前再同步一次数据，同步时停止平台运行服务

```bash
$ rsync -azvP /olddata/*   /newdata/
```

需要拷贝数据的组件：

db monitor grdata hub repo

- 删除控制器

使用新的数据运行

```bash
kubectl delete sts  rbd-db rbd-etcd rbd-monitor rbd-repo -n rbd-system
kubectl delete ds rbd-chaos rbd-gateway rbd-node  -n rbd-system
kubectl delete deploy rbd-api rbd-app-ui rbd-eventlog rbd-hub rbd-mq rbd-webcli rbd-worker  -n rbd-system
```

- 检查

pod处于运行状态时登录控制台，重启所有组件，检查存储是否正常，正常后删除旧NAS存储并清空数据

```bash
kubectl delete storageclass managed-nfs-storage
rm -rf /olddata/*
```